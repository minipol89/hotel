<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Customer;
use App\Entity\Room;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HotelFormController extends AbstractController
{
    /**
     * @Route("/form", name="form")
     */
    public function index(): Response
    {
        $manager = $this->getDoctrine()->getManager();
        $roomRepo = $manager->getRepository(Room::class);
        $rooms = $roomRepo->findAll();

        return $this->render('hotel_form/index.html.twig', [
            'controller_name' => 'HotelFormController',
            'rooms' => $rooms
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/add", name="add")
     */
    public function save(Request $request): Response
    {
        $manager = $this->getDoctrine()->getManager();
        $customer = new Customer();
        $customer->setFirstname($request->get("firstname"))
            ->setLastname($request->get("lastname"))
            ->setAddress($request->get("address"));

        $roomRepo = $manager->getRepository(Room::class);
        $room = $roomRepo->find($request->get("roomtype"));

        $booking = new Booking();
        $booking->setArrivalDate($request->get("arrivalDate"))
            ->setDepartureDate($request->get("departureDate"))
            ->setCustomer($customer)
            ->setRoom($room);

        $manager->persist($customer);
        $manager->persist($booking);
        $manager->flush();
        return $this->render('home_page/index.html.twig', [
            'controller_name' => 'HotelFormController',
        ]);
    }
}
