<?php

namespace App\Controller;

use App\Entity\Booking;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageViewController extends AbstractController
{
    /**
     * @Route("/view", name="homepage_view")
     */
    public function index(): Response
    {
        $manager = $this->getDoctrine()->getManager();
        $bookingRepo = $manager->getRepository(Booking::class);
        $bookings = $bookingRepo->findAll();

        return $this->render('homepage_view/index.html.twig', [
            'controller_name' => 'HomepageViewController',
            'bookings' => $bookings
        ]);
    }
}
